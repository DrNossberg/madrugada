using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour {

    private PlayerInputActions playerInputActions;
    private Vector2         inputMovement;
    private float           currentSteerAngle;
    private float           currentbreakForce;
    private bool            isBreaking;

    [SerializeField] private float motorForce;
    [SerializeField] private float breakForce;
    [SerializeField] private float maxSteerAngle;

    [SerializeField] private WheelCollider frontLeftWheelCollider;
    [SerializeField] private WheelCollider frontRightWheelCollider;
    [SerializeField] private WheelCollider rearLeftWheelCollider;
    [SerializeField] private WheelCollider rearRightWheelCollider;

    [SerializeField] private Transform frontLeftWheelTransform;
    [SerializeField] private Transform frontRightWheeTransform;
    [SerializeField] private Transform rearLeftWheelTransform;
    [SerializeField] private Transform rearRightWheelTransform;



    void Awake() {
        // playerInputActions = new PlayerInputActions();
    }

    private void FixedUpdate() {
        inputMovement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        HandleMotor();
        HandleSteering();
        UpdateWheels();
    }

    // private void OnEnable() {
    //     playerInputActions.Player.Enable();
    // }

    // private void OnDisable() {
    //     playerInputActions.Player.Disable();
    // }

    // private void Update() {
    //     if (Mouse.current.leftButton.wasPressedThisFrame) {
    //         Debug.Log("clicked");
    //     }
    // }

    public void OnMovement(InputAction.CallbackContext context) {
        // context.ReadValue<Vector2>();
        // Debug.Log("inputMovement" + inputMovement);
        // Debug.Log("Horizontal " + Input.GetAxis("Horizontal") + " Vertical " + Input.GetAxis("Vertical"));
    }

    public void OnJump(InputAction.CallbackContext context) {
        // if (context.started)
        //     isBreaking = true;
        // if (context.canceled)
        //     isBreaking = false;
    }

    private void HandleMotor() {
        frontLeftWheelCollider.motorTorque = inputMovement.y * motorForce;
        frontRightWheelCollider.motorTorque = inputMovement.y * motorForce;
        // Debug.Log("frontLeftWheelCollider.motorTorque" + frontLeftWheelCollider.motorTorque + "frontRightWheelCollider.motorTorque)" + frontRightWheelCollider.motorTorque);
        currentbreakForce = isBreaking ? breakForce : 0f;
        ApplyBreaking();       
    }

    private void ApplyBreaking() {
        frontRightWheelCollider.brakeTorque = currentbreakForce;
        frontLeftWheelCollider.brakeTorque = currentbreakForce;
        rearLeftWheelCollider.brakeTorque = currentbreakForce;
        rearRightWheelCollider.brakeTorque = currentbreakForce;
    }

    private void HandleSteering() {
        currentSteerAngle = maxSteerAngle * inputMovement.x;
        frontLeftWheelCollider.steerAngle = currentSteerAngle;
        frontRightWheelCollider.steerAngle = currentSteerAngle;
    }

    private void UpdateWheels() {
        UpdateWheel(frontLeftWheelCollider, frontLeftWheelTransform);
        UpdateWheel(frontRightWheelCollider, frontRightWheeTransform);
        UpdateWheel(rearRightWheelCollider, rearRightWheelTransform);
        UpdateWheel(rearLeftWheelCollider, rearLeftWheelTransform);
    }

    private void UpdateWheel(WheelCollider wheelCollider, Transform wheelTransform) {
        Vector3 pos;
        Quaternion rot;

        wheelCollider.GetWorldPose(out pos, out rot);
        wheelTransform.rotation = rot;
        wheelTransform.position = pos;
    }
}